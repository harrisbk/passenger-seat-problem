# passenger-seat-problem

from @djstrouse on Twitter:
"Fun probability question. 100 passengers board plane. 1st forgets seat assignment and so takes random seat. Subsequent passengers take assigned seat if available, random seat if not. What’s the probability that the final passenger sits in his/her assigned seat?"