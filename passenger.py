import random as r


class Seat:
    # object to represent a seat on an airplane, if someone is seated in it; if so, who?
    def __init__(self, ident):

        self.id = ident
        self.filled = False
        self.passenger = 0


class Experiment:
    # represents the scenario that 100 people board a plane, and the first sits in a random seat instead of her
    # assigned seat. every subsequent passenger sits in her own seat, if available, and a random one if unavailable

    def __init__(self):

        self.passengers = []
        self.seats = []

    def create_seats(self):

        for i in range(0, 100):

            self.seats.append(Seat(i))

    def assign_random(self):

        random_seat = r.choice(self.seats)

        while random_seat.filled:

            random_seat = r.choice(self.seats)

        return random_seat.id

    def assign_passenger_one(self):

        ident = self.assign_random()

        self.seats[ident].passenger = 0
        self.seats[ident].filled = True

    def assign_remaining_passengers(self):

        for i in range(1, 100):

            if self.seats[i].filled:

                ident = self.assign_random()
                self.seats[ident].passenger = i
                self.seats[ident].filled = True

            else:

                self.seats[i].passenger = i
                self.seats[i].filled = True

    def show_results(self):
        # return 1 if the last passenger sits in her assigned seat. return 0 if the last passenger sits in a seat
        # different than the one assigned
        if self.seats[99].passenger == 99:
            return 1
        else:
            return 0


class ExperimentArray:
    # run the experiment a specific number of times, and return the ratio of proper last sits to improper last sits.
    def __init__(self, count):

        self.count = count
        self.experiments = []

    def run_experiments(self):

        for i in range(0,self.count):

            exp = Experiment()
            exp.create_seats()
            exp.assign_passenger_one()
            exp.assign_remaining_passengers()
            self.experiments.append(exp.show_results())

    def print_ratio(self):

        count1 = 0
        for exp in self.experiments:
            if exp == 1:
                count1 += 1

        print float(count1)/float(self.count)


if __name__ == "__main__":
    exp_arr = ExperimentArray(1000000)
    exp_arr.run_experiments()
    exp_arr.print_ratio()


